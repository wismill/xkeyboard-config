<!--
Thank you for your interest!
Please consider using one of our MR templates for a better experience

# Limitation of the project resources

This project does not have the resources to implement every layout or feature,
so please consider creating a merge request instead. If you do not, keep in mind
that we cannot promise your request will be accepted nor commit to any deadline.
-->

# Description

(please fill in)

# Rationale for the inclusion in this project

<!--
# Request for addition of a new layout or other feature

As a general rule, we accept layouts that are part of official standards but
not experimental layouts.

For any feature, please provide evidences that it is popular enough to make sense
to include by default in every system that use xkeyboard-config. Please note that
it is in your interest to provide neutral and accessible sources, if possible in
English.
-->

(please fill in)
